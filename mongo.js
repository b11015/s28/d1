// MongoDB Atlas?
	/*
		>> MongoDB in the cloud. It is a cloud service of MongoDB, leading noSQL database
	*/
// Robo3T
	/*
		>> this is application which allows the use of GUI to maipulate MongoDB databases.

	*/

// Shell
	/*
		>> this is an interface to input commands and perform  CRUD operations in our database
	*/

// CRUD Operations
	// Create (insert), Read (select), Update and Delete

// CREATE - inserting documents
	// insertOne({})
	// insertMany([{}])
/*
	Syntax:
		db.collection.insertOne({
			fieldA: "value A",
			fieldB: "value B"
		})
*/

	db.users.insertOne({
		firstName: "Jean",
		lastName: "Smith",
		age: 24,
		contact: {
			phone: "09123456789",
			email: "js@mail.com"
		},
		courses: ["CSS", "Javascript", "ReactJS"],
		department: "none"
	})

// insertMany({})

	db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "09123456789",
				email: "sh@mail.com"
			},
			courses: ["Python", "PHP", "ReactJS"],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "09123456789",
				email: "na@mail.com"
			},
			courses: ["Laravel", "Sass", "Springboot"],
			department: "none"
		}
	])

// READ - retrieval of documents
	/*
		Syntax:
			>> return all documents
			db.collection.find()

			>> return first documents
			db.collection.find({})

			>> return ALL documents that matches a criteria in db.collection.find({criteria: "value"})

			>> return FIRST matching documents that matches the criteria
			db.collection.findOne({criteria: 'value'})

			>> return the first ALL documents that matches the criteria
			db.collection.find({criteriaA: 'valueaA', criteriaB: 'valueB', .....})
	*/

	// return all users
	db.users.find()

	// return first user
	db.users.findOne({})

	// return all users with phone department none
	db.users.find({department: "none"})

	// return all users enrolled in reactJS
	db.users.find({courses: 'ReactJS'})

	// return first matching user documents
	db.users.findOne({department: 'none'})

	// return ALL that matches the multiple criteria
	db.users.find({lastName: 'Armstrong', age: 82})

	// array as criteria
	db.users.find({courses: {$all: ["Python", "ReactJS", "PHP"]}})

	// object as criteria
	db.users.find({contact: {phone: "09123456789", email: "na@mail.com"}})

	// object nested using dot notation
	db.users.find({"contact.phone": "09123456789"})

// UPDATE
	/*
		>> update first document that matches the criteria
		
		>> one-liner
		db.collection.updateOne({criteria: "value"}, {$set:{"fieldToBeUpdated" : "updatedValue"}})

		>> to update the first item
		db.collection.updateOne({}, {$set: {fieldToBeUpdated : "updatedValue"}})

		>> to update ALL items in the collection
		db.collection.updateMany({}, {$set: {fieldToBeUpdated: "updatedValue"}})

		>> to update items that matches criteria
		db.collection.updateMany({criteria: "value"}, {$set: {fieldToBeUpdated: "value"}})

	*/

	// carol lastName change
	db.users.updateOne({firstName: "Carol"}, {$set: {"lastName" : "Marvel"}})


	db.users.updateOne({}, {$set: {emergencyContact: "father"}})	
	db.users.updateMany({}, {$set: {emergencyContact: "mother"}})

	db.users.updateMany({department: "none"}, {$set: {department: "tech"}})

	db.users.updateOne({}, {$rename: {"department": "dept"}})

// DELETE
	/*
		Syntax:
			>> delete the first item that matches the criteria
			db.collection.deleteOne({criteria: "value"})

			>> delete ALL items in that matches the criteria
			db.collection.deleteMany({criteria: "value"})

			>> delete the first item in the collection
			db.colletion.deleteOne({})

			>> delete ALL items in the collection
			db.collection.deleteMany({})
	*/ 

	// delete neil name
	db.users.deleteOne({firstName: "Neil"})

	// delete first item
	db.users.deleteOne({})

	// delete all armstrong lastName
	db.users.deleteMany({lastName: "Armstrong"})

	// delete all items - Fetched 0 records
	db.users.deleteMany({})